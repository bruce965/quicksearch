﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using QuickSearch.Lib;

namespace QuickSearch
{
	public partial class MainForm : Form
	{
		public MainForm() {
			InitializeComponent();
			
			Text += " v" + Assembly.GetExecutingAssembly().GetName().Version;
			
			linkize(labelLink, labelLink.Text);
		}
		
		static void linkize(Label label, string target) {
			label.Font = new Font(label.Font, FontStyle.Underline);
			label.ForeColor = Color.Blue;
			label.Cursor = Cursors.Hand;
			
			label.Click += (sender, e) => {
				try {
					Process.Start(target);
				} catch (Win32Exception) { }
			};
		}
		
		void browsePath(object sender, EventArgs e) {
			if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
				textBoxPath.Text = folderBrowserDialog1.SelectedPath;
		}
		
		void addPath(object sender, EventArgs e) {
			if (folderBrowserDialog1.ShowDialog() == DialogResult.OK) {
				if (textBoxPath.Text.Length > 0)
					textBoxPath.Text += "|" + folderBrowserDialog1.SelectedPath;
				else
					textBoxPath.Text = folderBrowserDialog1.SelectedPath;
			}
		}
		
		void searchClick(object sender, EventArgs e) {
			if (buttonSearch.Text == "Stop") {
				searchStop();
			} else {
				searchStart();
			}
		}
		
		CancellationTokenSource tokenSource;
		
		void searchStart() {
			buttonSearch.Text = "Stop";
			dataGridViewResults.Rows.Clear();
			
			tokenSource = new CancellationTokenSource();
			Search.RunAsync(new Settings {
				Paths = textBoxPath.Text.Split(new[] { '|' }).Select(path => path.Trim()).Where(path => !String.IsNullOrEmpty(path)).ToList(),
				Query = new Regex(makeQuery(), RegexOptions.Compiled|RegexOptions.CultureInvariant),
				DirectoryFilter = null,
				FileFilter = null,
				Recursive = true,
				SearchNames = true,
				SearchContents = true,
				MinCreationDate = null,
				MaxCreationDate = null,
				MinAccessDate = null,
				MaxAccessDate = null,
				MinModificationDate = null,
				MaxModificationDate = null,
				MinSize = null,
				MaxSize = null
			}, tokenSource.Token, match => Invoke((Action)(() => addResult(match))))
				.ContinueWith(task => {
					tokenSource.Dispose();
					tokenSource = null;
					Invoke((Action)searchStop);
					
					if (task.Exception != null)
						MessageBox.Show(task.Exception.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				});
		}
		
		void searchStop() {
			buttonSearch.Text = "Search";
			
			if (tokenSource != null) {
				tokenSource.Cancel();
				tokenSource.Dispose();
			}
		}
		
		string makeQuery() {
			switch (dropDownEnumQueryType.SelectedEnumValue) {
			case PatternType.ExactMatch:
				return Regex.Escape(textBoxQuery.Text);
			case PatternType.RegularExpression:
				return textBoxQuery.Text;
			case PatternType.Wildcard:
				return Regex.Escape(textBoxQuery.Text).Replace("\\*", ".*").Replace("\\?", ".");
			}
			
			throw new Exception("Unexpected status: PatternType=" + dropDownEnumQueryType.SelectedEnumValue);
		}
		
		void addResult(IMatch match) {
			var row = dataGridViewResults.Rows[dataGridViewResults.Rows.Add()];
			row.Cells["File"].Value = match.File.Name;
			row.Cells["Path"].Value = match.File.FullName;
			row.Cells["Line"].Value = match.Row == -1 ? (object)"" : (Int32)match.Row;
			row.Cells["FileSize"].Value = match.File is FileInfo ? ((FileInfo)match.File).Length : (object)"";
			row.Cells["CreationDate"].Value = match.File.CreationTime;
			row.Cells["AccessDate"].Value = match.File.LastAccessTime;
			row.Cells["ModificationDate"].Value = match.File.LastWriteTime;
			row.Cells["Value"].Value = match.Line == null ? "" : match.Line.Trim();
		}
		
		void cellDoubleClick(object sender, DataGridViewCellEventArgs e) {
			var row = dataGridViewResults.Rows[e.RowIndex];
			var path = (string)row.Cells["Path"].Value;
			var line = row.Cells["Line"].Value is Int32 ? (Int32)row.Cells["Line"].Value : -1;
			
			if (Directory.Exists(path))
				Process.Start(path);
			else
				Process.Start("notepad++", "-n" + line + " " + path);
		}
		
		void TextBoxQueryKeyUp(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Enter) {
				searchStart();
				buttonSearch.Focus();
			}
		}
		
		void TextBoxQueryTextChanged(object sender, EventArgs e) {
			var ss = textBoxQuery.SelectionStart;
			var sl = textBoxQuery.SelectionLength;
			textBoxQuery.Text = textBoxQuery.Text.Replace("\n", "").Replace("\r", "");
			textBoxQuery.SelectionStart = ss;
			textBoxQuery.SelectionLength = sl;
		}
	}
}
