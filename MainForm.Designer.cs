﻿/*
 * Created by SharpDevelop.
 * User: giopla
 * Date: 2016-09-19
 * Time: 10:59
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace QuickSearch
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxPath;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBoxQuery;
		private System.Windows.Forms.Label label2;
		private Advanced.Windows.Forms.DropDownEnum<QuickSearch.PatternType> dropDownEnumQueryType;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button buttonSearch;
		private System.Windows.Forms.Label labelLink;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DataGridView dataGridViewResults;
		private System.Windows.Forms.DataGridViewTextBoxColumn File;
		private System.Windows.Forms.DataGridViewTextBoxColumn Path;
		private System.Windows.Forms.DataGridViewTextBoxColumn Line;
		private System.Windows.Forms.DataGridViewTextBoxColumn FileSize;
		private System.Windows.Forms.DataGridViewTextBoxColumn CreationDate;
		private System.Windows.Forms.DataGridViewTextBoxColumn AccessDate;
		private System.Windows.Forms.DataGridViewTextBoxColumn ModificationDate;
		private System.Windows.Forms.DataGridViewTextBoxColumn Value;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxPath = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.textBoxQuery = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.dropDownEnumQueryType = new Advanced.Windows.Forms.DropDownEnum<QuickSearch.PatternType>();
			this.button4 = new System.Windows.Forms.Button();
			this.buttonSearch = new System.Windows.Forms.Button();
			this.labelLink = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.dataGridViewResults = new System.Windows.Forms.DataGridView();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.File = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Path = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Line = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FileSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.CreationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.AccessDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ModificationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewResults)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 14);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Path(s):";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBoxPath
			// 
			this.textBoxPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxPath.Location = new System.Drawing.Point(79, 14);
			this.textBoxPath.Name = "textBoxPath";
			this.textBoxPath.Size = new System.Drawing.Size(531, 20);
			this.textBoxPath.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(697, 12);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 2;
			this.button1.Text = "Add...";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.addPath);
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.Location = new System.Drawing.Point(616, 12);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 3;
			this.button2.Text = "Browse...";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.browsePath);
			// 
			// textBoxQuery
			// 
			this.textBoxQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxQuery.Location = new System.Drawing.Point(79, 41);
			this.textBoxQuery.Multiline = true;
			this.textBoxQuery.Name = "textBoxQuery";
			this.textBoxQuery.Size = new System.Drawing.Size(531, 20);
			this.textBoxQuery.TabIndex = 5;
			this.textBoxQuery.TextChanged += new System.EventHandler(this.TextBoxQueryTextChanged);
			this.textBoxQuery.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBoxQueryKeyUp);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 41);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 20);
			this.label2.TabIndex = 4;
			this.label2.Text = "Query:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dropDownEnumQueryType
			// 
			this.dropDownEnumQueryType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.dropDownEnumQueryType.Location = new System.Drawing.Point(616, 41);
			this.dropDownEnumQueryType.Name = "dropDownEnumQueryType";
			this.dropDownEnumQueryType.SelectedEnumValue = QuickSearch.PatternType.ExactMatch;
			this.dropDownEnumQueryType.Size = new System.Drawing.Size(156, 21);
			this.dropDownEnumQueryType.TabIndex = 6;
			// 
			// button4
			// 
			this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button4.Location = new System.Drawing.Point(591, 55);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 7;
			this.button4.Text = "Set Filter";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Visible = false;
			// 
			// buttonSearch
			// 
			this.buttonSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSearch.Location = new System.Drawing.Point(616, 68);
			this.buttonSearch.Name = "buttonSearch";
			this.buttonSearch.Size = new System.Drawing.Size(156, 23);
			this.buttonSearch.TabIndex = 8;
			this.buttonSearch.Text = "Search";
			this.buttonSearch.UseVisualStyleBackColor = true;
			this.buttonSearch.Click += new System.EventHandler(this.searchClick);
			// 
			// labelLink
			// 
			this.labelLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.labelLink.Location = new System.Drawing.Point(454, 68);
			this.labelLink.Name = "labelLink";
			this.labelLink.Size = new System.Drawing.Size(156, 23);
			this.labelLink.TabIndex = 9;
			this.labelLink.Text = "https://www.fabioiotti.com/";
			this.labelLink.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(12, 68);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(156, 23);
			this.label3.TabIndex = 10;
			this.label3.Text = "Copyright © 2016 Fabio Iotti";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dataGridViewResults
			// 
			this.dataGridViewResults.AllowUserToAddRows = false;
			this.dataGridViewResults.AllowUserToDeleteRows = false;
			this.dataGridViewResults.AllowUserToOrderColumns = true;
			this.dataGridViewResults.AllowUserToResizeRows = false;
			this.dataGridViewResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridViewResults.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dataGridViewResults.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dataGridViewResults.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dataGridViewResults.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridViewResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewResults.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.File,
			this.Path,
			this.Line,
			this.FileSize,
			this.CreationDate,
			this.AccessDate,
			this.ModificationDate,
			this.Value});
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(4, 0, 4, 0);
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewResults.DefaultCellStyle = dataGridViewCellStyle2;
			this.dataGridViewResults.Location = new System.Drawing.Point(0, 97);
			this.dataGridViewResults.Name = "dataGridViewResults";
			this.dataGridViewResults.ReadOnly = true;
			this.dataGridViewResults.RowHeadersVisible = false;
			this.dataGridViewResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewResults.Size = new System.Drawing.Size(784, 464);
			this.dataGridViewResults.TabIndex = 11;
			this.dataGridViewResults.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.cellDoubleClick);
			// 
			// folderBrowserDialog1
			// 
			this.folderBrowserDialog1.ShowNewFolderButton = false;
			// 
			// File
			// 
			this.File.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.File.FillWeight = 60F;
			this.File.HeaderText = "File";
			this.File.Name = "File";
			this.File.ReadOnly = true;
			// 
			// Path
			// 
			this.Path.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.Path.HeaderText = "Path";
			this.Path.Name = "Path";
			this.Path.ReadOnly = true;
			// 
			// Line
			// 
			this.Line.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.Line.FillWeight = 40F;
			this.Line.HeaderText = "Line";
			this.Line.Name = "Line";
			this.Line.ReadOnly = true;
			// 
			// FileSize
			// 
			this.FileSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.FileSize.FillWeight = 40F;
			this.FileSize.HeaderText = "Size";
			this.FileSize.Name = "FileSize";
			this.FileSize.ReadOnly = true;
			// 
			// CreationDate
			// 
			this.CreationDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.CreationDate.FillWeight = 40F;
			this.CreationDate.HeaderText = "Creation Date";
			this.CreationDate.Name = "CreationDate";
			this.CreationDate.ReadOnly = true;
			// 
			// AccessDate
			// 
			this.AccessDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.AccessDate.FillWeight = 40F;
			this.AccessDate.HeaderText = "Access Date";
			this.AccessDate.Name = "AccessDate";
			this.AccessDate.ReadOnly = true;
			// 
			// ModificationDate
			// 
			this.ModificationDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ModificationDate.FillWeight = 40F;
			this.ModificationDate.HeaderText = "Modification Date";
			this.ModificationDate.Name = "ModificationDate";
			this.ModificationDate.ReadOnly = true;
			// 
			// Value
			// 
			this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.Value.FillWeight = 150F;
			this.Value.HeaderText = "Text";
			this.Value.Name = "Value";
			this.Value.ReadOnly = true;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 561);
			this.Controls.Add(this.dataGridViewResults);
			this.Controls.Add(this.labelLink);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.buttonSearch);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.dropDownEnumQueryType);
			this.Controls.Add(this.textBoxQuery);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.textBoxPath);
			this.Controls.Add(this.label1);
			this.MinimumSize = new System.Drawing.Size(600, 400);
			this.Name = "MainForm";
			this.Text = "Quick Search";
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewResults)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
