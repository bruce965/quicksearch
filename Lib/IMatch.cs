﻿using System;
using System.IO;

namespace QuickSearch.Lib
{
	public interface IMatch
	{
		/// <summary>The matched file or directory.</summary>
		FileSystemInfo File { get; }
		
		/// <summary>The row the match starts at (-1 if matched by name).</summary>
		int Row { get; }
		
		/// <summary>The column the match starts at (-1 if matched by name).</summary>
		int Column { get; }
		
		string Line { get; }
	}
}
