﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using QuickSearch.Lib;

namespace QuickSearch.Lib
{
	public static class Search
	{
		class Match : IMatch {
			readonly FileSystemInfo file;
			readonly int row;
			readonly int column;
			readonly string rowText;
			
			FileSystemInfo IMatch.File { get { return file; } }
			int IMatch.Row { get { return row; } }
			int IMatch.Column { get { return column; } }
			string IMatch.Line { get { return rowText; } }
			
			public Match(FileSystemInfo directory) {
				this.file = directory;
				this.row = -1;
				this.column = -1;
			}
			
			public Match(FileInfo file, int row, int column, string rowText) {
				this.file = file;
				this.row = row;
				this.column = column;
				this.rowText = rowText;
			}
		}
		
		public static Task RunAsync(ISearchSettings settings, CancellationToken token, Action<IMatch> onMatch) {
			return Task.Factory.StartNew(() => {
				foreach (var match in Run(settings, token))
					onMatch(match);
			}, token);
		}
		
		public static IEnumerable<IMatch> Run(ISearchSettings settings, CancellationToken token) {
			foreach (var path in settings.Paths) {
				if (token.IsCancellationRequested)
					yield break;
				
				if (Directory.Exists(path)) {
					DirectoryInfo directory;
					try {
						directory = new DirectoryInfo(path);
					} catch (SecurityException) {
						// TODO: path not accessible, report somehow.
						continue;
					} catch (ArgumentException) {
						// TODO: path not valid, report somehow.
						continue;
					} catch (PathTooLongException) {
						// TODO: path too long, report somehow.
						continue;
					}
					
					foreach (var match in searchDirectory(directory, settings, token)) {
						if (token.IsCancellationRequested)
							yield break;
						
						yield return match;
					}
				} else if (File.Exists(path)) {
					FileInfo file;
					try {
						file = new FileInfo(path);
					} catch (SecurityException) {
						// TODO: path not accessible, report somehow.
						continue;
					} catch (ArgumentException) {
						// TODO: path not valid, report somehow.
						continue;
					} catch (UnauthorizedAccessException) {
						// TODO: path not accessible, report somehow.
						continue;
					} catch (PathTooLongException) {
						// TODO: path too long, report somehow.
						continue;
					} catch (NotSupportedException) {
						// TODO: path not valid, report somehow.
						continue;
					}
					
					foreach (var match in searchFile(file, settings, token)) {
						if (token.IsCancellationRequested)
							yield break;
						
						yield return match;
					}
				}
			}
		}
		
		static IEnumerable<IMatch> searchDirectory(DirectoryInfo directory, ISearchSettings settings, CancellationToken token) {
			if (token.IsCancellationRequested)
				yield break;
			
			if (!directory.Exists)
				yield break;
			
			if (!settings.DirectoryFilter.IsMatch(directory))
				yield break;
			
			if (settings.SearchNames && settings.Query.IsMatch(directory.Name))
				yield return new Match(directory);
			
			IEnumerable<FileInfo> files;
			IEnumerable<DirectoryInfo> directories;
			try {
				files = directory.EnumerateFiles();
				directories = directory.EnumerateDirectories();
			} catch (SecurityException) {
				// TODO: path not accessible, report somehow.
				yield break;
			} catch (DirectoryNotFoundException) {
				// NOTE: happens even if `directory.Exists` returned `true` if directory is deleted before this line is reached.
				yield break;
			} catch (UnauthorizedAccessException) {
				// TODO: path not accessible, report somehow.
				yield break;
			}
			
			foreach (var file in files) {
				foreach (var match in searchFile(file, settings, token)) {
					if (token.IsCancellationRequested)
						yield break;
					
					yield return match;
				}
			}
			
			foreach (var directory2 in directories) {
				foreach (var match in searchDirectory(directory2, settings, token)) {
					if (token.IsCancellationRequested)
						yield break;
					
					yield return match;
				}
			}
		}
		
		static IEnumerable<IMatch> searchFile(FileInfo file, ISearchSettings settings, CancellationToken token) {
			if (token.IsCancellationRequested)
				yield break;
			
			if (!file.Exists)
				yield break;
			
			if (!settings.FileFilter.IsMatch(file))
				yield break;
			
			if (settings.SearchNames && settings.Query.IsMatch(file.Name))
				yield return new Match(file);
			
			if (settings.SearchContents) {
				StreamReader reader;
				try {
					reader = File.OpenText(file.FullName);
				} catch (UnauthorizedAccessException) {
					// TODO: file not accessible, report somehow.
					yield break;
				} catch (IOException) {
					// TODO: file not accessible, report somehow.
					yield break;
				}
				
				
				int lineIndex = 0;
				while (true) {
					if (token.IsCancellationRequested)
						yield break;
					
					string line;
					try {
						line = reader.ReadLine();
						if (line == null)
							break;
					} catch (IOException) {
						// TODO: I/O error, report somehow.
						break;
					}
					
					lineIndex++;
					if (settings.Query.IsMatch(line)) {
						var match = settings.Query.Match(line);
						yield return new Match(file, lineIndex, match.Index, line);
					}
				}
				
				reader.Dispose();
			}
		}
	}
}
