﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace QuickSearch.Lib
{
	public class Settings : ISearchSettings
	{
		class Matcher : IMatcher<FileSystemInfo> {
			readonly Settings settings;
			readonly bool file;
			
			IMatcher<string> filter {
				get { return file ? settings.FileFilter : settings.DirectoryFilter; }
			}
			
			public Matcher(Settings settings, bool file) {
				this.settings = settings;
				this.file = file;
			}

			public bool IsMatch(FileSystemInfo value) {
				if (filter != null && !filter.IsMatch(value.FullName))
					return false;
				
				if (settings.MinCreationDate.HasValue || settings.MaxCreationDate.HasValue) {
					try {
						var creationDate = value.CreationTimeUtc;
						
						if (settings.MinCreationDate.HasValue && creationDate < settings.MinCreationDate)
							return false;
						
						if (settings.MaxCreationDate.HasValue && creationDate > settings.MaxCreationDate)
							return false;
					} catch (IOException) {
					//} catch (DirectoryNotFoundException) {
					} catch (PlatformNotSupportedException) { }
				}
				
				if (settings.MinAccessDate.HasValue || settings.MaxAccessDate.HasValue) {
					try {
						var accessDate = value.LastAccessTimeUtc;
						
						if (settings.MinAccessDate.HasValue && accessDate < settings.MinAccessDate)
							return false;
						
						if (settings.MaxAccessDate.HasValue && accessDate > settings.MaxAccessDate)
							return false;
					} catch (IOException) {
					//} catch (DirectoryNotFoundException) {
					} catch (PlatformNotSupportedException) { }
				}
				
				if (settings.MinModificationDate.HasValue || settings.MaxModificationDate.HasValue) {
					try {
						var modificationDate = value.LastWriteTimeUtc;
						
						if (settings.MinModificationDate.HasValue && modificationDate < settings.MinModificationDate)
							return false;
						
						if (settings.MaxModificationDate.HasValue && modificationDate > settings.MaxModificationDate)
							return false;
					} catch (IOException) {
					//} catch (DirectoryNotFoundException) {
					} catch (PlatformNotSupportedException) { }
				}
			
				if (file && (settings.MinSize.HasValue || settings.MaxSize.HasValue)) {
					try {
						var size = (value as FileInfo).Length;
						
						if (settings.MinSize.HasValue && size < settings.MinSize)
							return false;
						
						if (settings.MaxSize.HasValue && size > settings.MaxSize)
							return false;
					} catch (IOException) { }
					//} catch (FileNotFoundException) { }
				}
				
				return true;
			}
		}
		
		public List<string> Paths { get; set; }
		
		IEnumerable<string> ISearchSettings.Paths {
			get { return Paths; }
		}
		
		readonly Matcher directoryMatcher;
		IMatcher<DirectoryInfo> ISearchSettings.DirectoryFilter {
			get { return directoryMatcher; }
		}
		
		readonly Matcher fileMatcher;
		IMatcher<FileInfo> ISearchSettings.FileFilter {
			get { return fileMatcher; }
		}
		
		public Regex Query { get; set; }
		public IMatcher<string> DirectoryFilter { get; set; }
		public IMatcher<string> FileFilter { get; set; }
		
		public bool Recursive { get; set; }
		
		public bool SearchNames { get; set; }
		
		public bool SearchContents { get; set; }
		
		public DateTime? MinCreationDate { get; set; }
		public DateTime? MaxCreationDate { get; set; }
		public DateTime? MinAccessDate { get; set; }
		public DateTime? MaxAccessDate { get; set; }
		public DateTime? MinModificationDate { get; set; }
		public DateTime? MaxModificationDate { get; set; }
		public int? MinSize { get; set; }
		public int? MaxSize { get; set; }
		
		public Settings() {
			directoryMatcher = new Matcher(this, false);
			fileMatcher = new Matcher(this, true);
		}
	}
}
