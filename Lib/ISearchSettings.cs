﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace QuickSearch.Lib
{
	public interface ISearchSettings
	{
		/// <summary>List of paths to search in.</summary>
		IEnumerable<string> Paths { get; }
		
		/// <summary>Only include matching directories in search (optional).</summary>
		IMatcher<DirectoryInfo> DirectoryFilter { get; }
		
		/// <summary>Only include matching files in search (optional).</summary>
		IMatcher<FileInfo> FileFilter { get; }
		
		/// <summary>What to search for.</summary>
		Regex Query { get; }
		
		/// <summary>Recursively search in <see cref="Paths" />.</summary>
		bool Recursive { get; }
		
		/// <summary>Include file and directory names in search.</summary>
		bool SearchNames { get; }
		
		/// <summary>Include file contents in search.</summary>
		bool SearchContents { get; }
	}
}
