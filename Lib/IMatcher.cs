﻿using System;

namespace QuickSearch.Lib
{
	public interface IMatcher<in T>
	{
		bool IsMatch(T value);
	}
}
