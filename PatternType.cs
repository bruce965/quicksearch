﻿using System;

namespace QuickSearch
{
	public enum PatternType
	{
		ExactMatch,
		RegularExpression,
		Wildcard
	}
}
