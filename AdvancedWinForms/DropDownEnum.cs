﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Advanced.Windows.Forms
{
	//public class DropDownEnum<T> : ListControl where T : struct, IComparable, IConvertible, IFormattable
	public class DropDownEnum<T> : ComboBox where T : struct, IComparable, IConvertible, IFormattable
	{
		readonly Array enumValues = Enum.GetValues(typeof(T));
		
		//[Browsable(false)]
		public T SelectedEnumValue {
			get {
				try {
					return (T)enumValues.GetValue(SelectedIndex);
				} catch (IndexOutOfRangeException) {
					return default(T);
				}
			}
			
			set {
				for (var i = 0; i < Items.Count; i++) {
					if (value.Equals(Items[i])) {
						SelectedIndex = i;
						return;
					}
				}
				
				SelectedIndex = -1;
			}
		}
		
		public DropDownEnum() {
			#if DEBUG
			if (!typeof(T).IsEnum)	// check not possible at compile time
				throw new InvalidProgramException("T must be an Enum.");
			#endif
			
			if (LicenseManager.UsageMode == LicenseUsageMode.Runtime) {
				DropDownStyle = ComboBoxStyle.DropDownList;
				
				Items.Clear();
				foreach (var value in enumValues)
					Items.Add(value);
				
				SelectedEnumValue = default(T);
			}
		}
	}
}
